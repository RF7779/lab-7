#version 330 core

out vec4 fragColor;

/**
 * TODO: PART-1 update the fragment shader to get the texture coordinates from
 * the vertex shader
 */
in vec3 ourColor;
in vec2 TexCoord;

/**
 * TODO: PART-3 update the fragment shader to get the fragColor color from the
 * texture, and add the sampler2D.
 */

uniform sampler2D texture1;


void main() {
	//fragColor = vec4(uv.x, uv.y, 0, 1);	//what the assignment gives as example
    //fragColor = vec4(1, 0, 0, 1);
	//fragColor = vec4(ourColor, 1);	//my failed finagalings
	//fragColor = vec4(ourTexCoord, 0, 1);
	//fragColor = vec4(coords[0], coords[1], 0,1);
	fragColor = texture(texture1, TexCoord);
}